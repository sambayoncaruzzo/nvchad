require "nvchad.options"

-- Height to the aucompletion window
vim.api.nvim_set_option("pumheight", 5)

-- highlight yank
vim.cmd [[au TextYankPost * silent! lua vim.highlight.on_yank {higroup="IncSearch", timeout=250}]]

-- Relative numbers options
vim.opt.number = true
vim.opt.relativenumber = true

--Keep the cursor centered
vim.o.scrolloff = 999

-- add yours here!
if vim.g.neovide then
  vim.g.neovide_cursor_trail_size = 0.8
  vim.o.guifont = "FiraCode Nerd Font:h17"
  vim.g.neovide_remember_window_size = true
  vim.keymap.set("n", "<C-S-v>", '"+P') -- Paste normal mode
  vim.g.neovide_padding_top = 0
  vim.g.neovide_padding_bottom = 0
  vim.g.neovide_padding_right = 0
  vim.g.neovide_padding_left = 0
end
-- local o = vim.o
-- o.cursorlineopt ='both' -- to enable cursorline!
