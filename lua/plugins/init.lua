return {
  {
    "stevearc/conform.nvim",
    event = "BufWritePre", -- uncomment for format on save
    config = function()
      require "configs.conform"
    end,
  },

  -- {
  --   "williamboman/mason.nvim",
  -- },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "vim",
        "lua",
        "vimdoc",
        "html",
        "css",
        "json",
        "javascript",
        "typescript",
        "graphql",
        "tsx",
        "go",
        "gosum",
      },
    },
  },

  {
    "neovim/nvim-lspconfig",

    config = function()
      require "configs.lspconfig"
    end,
  },
  {
    "mfussenegger/nvim-lint",
    event = {
      "BufReadPre",
      "BufNewFile",
    },
    config = function()
      require("nvchad.configs.lspconfig").defaults()
      require "configs.lint"
    end,
  },

  {
    "lewis6991/gitsigns.nvim",
    opts = {
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map("n", "]c", function()
          if vim.wo.diff then
            return "]c"
          end
          vim.schedule(function()
            gs.next_hunk()
          end)
          return "<Ignore>"
        end, { expr = true, desc = "Next Hunk" })

        map("n", "[c", function()
          if vim.wo.diff then
            return "[c"
          end
          vim.schedule(function()
            gs.prev_hunk()
          end)
          return "<Ignore>"
        end, { expr = true, desc = "Prev Hunk" })
        map("n", "<leader>ph", gs.preview_hunk, { desc = "Preview Hunk" })
      end,
    },
  },
  {
    "windwp/nvim-ts-autotag",
    ft = {

      "html",
      "javascript",
      "typescript",
      "javascriptreact",
      "typescriptreact",
      "tsx",
      "jsx",
      "rescript",
      "css",
      "xml",
    },
    config = function()
      require("nvim-ts-autotag").setup {
        filetypes = {
          "html",
          "javascript",
          "typescript",
          "javascriptreact",
          "typescriptreact",
          "tsx",
          "jsx",
          "rescript",
          "css",
          "xml",
        },
      }
    end,
  },

  {
    "dgagn/diagflow.nvim",
    lazy = false,
    opts = {},
  },

  {
    "NeogitOrg/neogit",
    cmd = "Neogit",
    dependencies = {
      "nvim-lua/plenary.nvim", -- required
      "nvim-telescope/telescope.nvim", -- optional
      "sindrets/diffview.nvim", -- optional
      "ibhagwan/fzf-lua", -- optional
    },
    config = function()
      require("neogit").setup {

        integrations = {
          -- If enabled, use telescope for menu selection rather than vim.ui.select.
          -- Allows multi-select and some things that vim.ui.select doesn't.
          telescope = true,
          -- Neogit only provides inline diffs. If you want a more traditional way to look at diffs, you can use `diffview`.
          -- The diffview integration enables the diff popup.
          --
          -- Requires you to have `sindrets/diffview.nvim` installed.
          diffview = true,

          -- If enabled, uses fzf-lua for menu selection. If the telescope integration
          -- is also selected then telescope is used instead
          -- Requires you to have `ibhagwan/fzf-lua` installed.
          fzf_lua = true,
        },
      }
    end,
  },

  {
    "gelguy/wilder.nvim",
    event = "CmdlineEnter",
    config = function()
      local wilder = require "wilder"
      wilder.setup { modes = { ":", "/", "?" } }
      wilder.set_option(
        "renderer",
        wilder.renderer_mux {
          [":"] = wilder.popupmenu_renderer {
            highlighter = wilder.basic_highlighter(),
            left = { " ", wilder.popupmenu_devicons() },
            right = { " ", wilder.popupmenu_scrollbar() },
          },
          ["/"] = wilder.wildmenu_renderer {
            highlighter = wilder.basic_highlighter(),
          },
        }
      )
    end,
  },

  {
    "stevearc/aerial.nvim",
    ft = {
      "go",
      "html",
      "javascript",
      "typescript",
      "javascriptreact",
      "typescriptreact",
      "tsx",
      "jsx",
      "rescript",
      "css",
    },
    opts = {},
    -- Optional dependencies
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-tree/nvim-web-devicons",
    },
  },

  {
    "dmmulroy/ts-error-translator.nvim",

    ft = {
      "typescript",
      "javascriptreact",
      "typescriptreact",
      "tsx",
      "jsx",
    },
    config = function()
      require("ts-error-translator").setup()
    end,
  },

  {
    "karb94/neoscroll.nvim",
    event = "BufEnter",
    config = function()
      require("neoscroll").setup {}
    end,
  },
  {
    "ray-x/go.nvim",
    dependencies = { -- optional packages
      "ray-x/guihua.lua",
      "neovim/nvim-lspconfig",
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      require("go").setup()
    end,
    ft = { "go", "gomod" },
    -- event = { "CmdlineEnter" },
    build = ':lua require("go.install").update_all_sync()', -- if you need to install/update all binaries
  },
  {
    "rachartier/tiny-code-action.nvim",
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      { "nvim-telescope/telescope.nvim" },
    },
    event = "LspAttach",
    config = function()
      require("tiny-code-action").setup {
        {
          --- The backend to use, currently only "vim" and "delta" are supported
          backend = "vim",
          backend_opts = {
            delta = {
              -- Header from delta can be quite large.
              -- You can remove them by setting this to the number of lines to remove
              header_lines_to_remove = 4,

              -- The arguments to pass to delta
              -- If you have a custom configuration file, you can set the path to it like so:
              -- args = {
              --     "--config" .. os.getenv("HOME") .. "/.config/delta/config.yml",
              -- }
              args = {
                "--line-numbers",
              },
            },
          },
          telescope_opts = {
            layout_strategy = "vertical",
            layout_config = {
              width = 0.7,
              height = 0.9,
              preview_cutoff = 1,
              preview_height = function(_, _, max_lines)
                local h = math.floor(max_lines * 0.5)
                return math.max(h, 10)
              end,
            },
          },
          -- The icons to use for the code actions
          -- You can add your own icons, you just need to set the exact action's kind of the code action
          -- You can set the highlight like so: { link = "DiagnosticError" } or  like nvim_set_hl ({ fg ..., bg..., bold..., ...})
          signs = {
            quickfix = { "󰁨", { link = "DiagnosticInfo" } },
            others = { "?", { link = "DiagnosticWarning" } },
            refactor = { "", { link = "DiagnosticWarning" } },
            ["refactor.move"] = { "󰪹", { link = "DiagnosticInfo" } },
            ["refactor.extract"] = { "", { link = "DiagnosticError" } },
            ["source.organizeImports"] = { "", { link = "TelescopeResultVariable" } },
            ["source.fixAll"] = { "", { link = "TelescopeResultVariable" } },
            ["source"] = { "", { link = "DiagnosticError" } },
            ["rename"] = { "󰑕", { link = "DiagnosticWarning" } },
            ["codeAction"] = { "", { link = "DiagnosticError" } },
          },
        },
      }
    end,
  },
}
