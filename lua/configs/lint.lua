local lint = require "lint"
lint.linters_by_ft = {
  --Eslint no es necesario configurarlo acá, ya que eslint-lsp ya hace ese trabajo.
  typescript = { "codespell" },
  typescriptreact = {
    "codespell",
  },
  javascript = { "quick-lint-js", "codespell" },
  go = { "golangcilint", "codespell" },
}

local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })

vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
  group = lint_augroup,
  callback = function()
    lint.try_lint()
  end,
})
