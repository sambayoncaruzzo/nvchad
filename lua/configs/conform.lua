local options = {
  formatters_by_ft = {
    lua = { "stylua" },
    -- css = { "prettier" },
    -- html = { "prettier" },
    -- Conform will run multiple formatters sequentially
    go = { "goimports", "gofmt", "gofumpt", "golines", stop_after_first = true },
    --  Conform will run only the first available formatter
    javascript = { "prettier", stop_after_first = true },
    typescript = { "prettier", stop_after_first = true },
    typescriptreact = { "prettier", stop_after_first = true },
    tsx = { "prettier", stop_after_first = true },
    css = { "prettier", stop_after_first = true },
    json = { "prettier", stop_after_first = true },
    graphql = { "prettier", stop_after_first = true },
    jsonc = { "prettier", stop_after_first = true },
    yaml = { "prettier", stop_after_first = true },
  },

  format_on_save = {
    -- These options will be passed to conform.format()
    timeout_ms = 500,
    lsp_fallback = true,
  },
}

require("conform").setup(options)
