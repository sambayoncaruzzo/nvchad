require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")

-- Paste in visual Mode Dont Replace the yank register
map("v", "<leader>p", '"_dP')

-- Aerial
map("n", "<leader>ta", "<cmd>AerialToggle<cr>", { desc = "Aerial " })
map("n", "<leader>tn", "<cmd>AerialNavToggle<cr>", { desc = "Aerial Nav" })
map("n", "[a", "<cmd>AerialPrev<cr>", { desc = "Aerial Prev" })
map("n", "]a", "<cmd>AerialNext<cr>", { desc = "Aerial Next" })

-- Code Actions Preview
map("n", "<leader>cp", function()
  require("tiny-code-action").code_action()
end, { desc = "Code Actions Preview", noremap = true, silent = true })
